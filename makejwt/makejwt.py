import jwt

with open('privkey.pem') as f:
    privkey = f.read()

claim = {
"iss": "https://auth.example.com",
"aud": "https://api.example.com",
"exp": 1585067190
}
enc = jwt.encode(claim, privkey, algorithm='RS256')
print(str(enc, 'utf-8'))
