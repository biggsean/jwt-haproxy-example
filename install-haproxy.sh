    5  curl -sLO https://www.haproxy.org/download/2.1/src/haproxy-2.1.0.tar.g
    7  curl -sLO https://www.lua.org/ftp/lua-5.3.5.tar.gz
    9  sudo yum -y install gcc openssl-devel readline-devel systemd-devel unzip
   11  tar xf lua-5.3.5.tar.gz
   12  cd lua-5.3.5/
   13  make linux test
   24  sudo make install
   25  lua -v
   26  cd ../
   28  tar xf haproxy-2.1.0.tar.gz 
   29  cd haproxy-2.1.0/
   31  make TARGET=linux-glibc USE_LINUX_SPLICE=1 USE_CPU_AFFINITY=1 USE_REGPARM=1 USE_SYSTEMD=1 USE_PCRE= USE_PCRE_JIT=1 USE_NS=1 USE_OPENSSL=1 USE_LUA=1 LUA_INC=./lua-5.3.5/src/ LUA_LIB=./lua-5.3.5/src/ 
   32  sudo cp haproxy /usr/sbin/
   34  sudo mkdir -p /etc/haproxy/pem
   38  haproxy -vvv|grep -i lua
   39  cd contrib/systemd/
   40  make clean
   41  make PREFIX=/usr
   42  cat haproxy.service
   43  sudo cp haproxy.service /usr/lib/systemd/system/
   44  sudo systemctl daemon-reload 
   45  cd
   46  curl -sLO https://github.com/diegonehab/luasocket/archive/master.zip
   48  unzip master.zip 
   49  curl -sLO https://github.com/rxi/json.lua/archive/master.zip
   50  unzip master.zip 
   51  curl -sLO https://github.com/wahern/luaossl/archive/master.zip
   52  unzip master.zip 
   53  cd luasocket-master/
   54  make clean all install-both LUAINC=/usr/include/lua5.3/
   55  sudo make clean all install-both LUAINC=/usr/include/lua5.3/
   56  cd ../luaossl-master/
   57  sudo make install
   58  cd ..
   59  cp json.lua-master/json.lua /usr/local/share/lua/5.3/
   60  sudo cp json.lua-master/json.lua /usr/local/share/lua/5.3/
   62  yum -y install epel-release
   63  sudo yum -y install epel-release
   65  sudo yum install git gcc zlib-devel bzip2-devel readline-devel sqlite-devel openssl-devel
   66  git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv
   67  vim $HOME/.bashrc 
   68  sudo yum install -y vim-enhanced
   69  vim $HOME/.bashrc 
   75  pyenv install 3.7.7
   76  pyenv global
   77  pyenv global 3.7.7
   78  pyenv global
   79  python --version
   80  curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
   81  source $HOME/.poetry/env
   82  vim $HOME/.bashrc 
